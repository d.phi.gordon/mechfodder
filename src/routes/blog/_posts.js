import all from './posts/*.md';

// all imports run through rollup first
// rollup applies processing as defined by
// plugins and configurations. 'md' files
// come precompiled.
export default all
  // replaces 3 tab characters at beginning of line/string with ''
  .map((post) => ({ ...post, html: post.html.replace(/^\t{3}/gm, '') }))
  // sorts by date
  .sort((a, b) => new Date(b.date) - new Date(a.date));
